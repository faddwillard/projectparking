﻿using System.Collections.Generic;

namespace ParkingClassLibrary
{
    public class Settings
    {
        public static int TimeoutInMilliseconds;
        public static int ParkingSpace;
        public static double Fine;
        public static Dictionary<CarType, double> Prices = new Dictionary<CarType, double>
        {
            [CarType.Passenger] = 2,
            [CarType.Truck] = 5,
            [CarType.Bus] = 3.5,
            [CarType.Motorcycle] = 1
        };

        static Settings()
        {
            //Milliseconds
            TimeoutInMilliseconds = 5000;
            ParkingSpace = 10;
            Fine = 2.5;
        }
    }
}
